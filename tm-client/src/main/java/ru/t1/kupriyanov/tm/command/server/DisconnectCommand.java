package ru.t1.kupriyanov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.command.AbstractCommand;
import ru.t1.kupriyanov.tm.enumerated.Role;

public class DisconnectCommand extends AbstractCommand {

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();
    }

    @Override
    public @Nullable String getName() {
        return "disconnect";
    }

    @Override
    public @Nullable Role[] getRoles() {
        return Role.values();
    }

    @Override
    public @Nullable String getDescription() {
        return "Disconnect from server.";
    }

}