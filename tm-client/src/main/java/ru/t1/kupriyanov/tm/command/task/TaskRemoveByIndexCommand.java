package ru.t1.kupriyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().removeTaskByIndex(new TaskRemoveByIndexRequest(index));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

}
