package ru.t1.kupriyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProject(new ProjectClearRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
