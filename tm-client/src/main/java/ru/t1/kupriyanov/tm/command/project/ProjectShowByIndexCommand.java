package ru.t1.kupriyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.kupriyanov.tm.dto.request.ProjectGetByIndexRequest;
import ru.t1.kupriyanov.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.kupriyanov.tm.dto.response.ProjectGetByIndexResponse;
import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(index);
        @NotNull final ProjectGetByIndexResponse response = getProjectEndpoint().getProjectByIndex(request);
        showProject(response.getProject());
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index.";
    }

}
