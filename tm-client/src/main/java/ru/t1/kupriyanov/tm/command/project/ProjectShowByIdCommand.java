package ru.t1.kupriyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.kupriyanov.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(id);
        @NotNull final ProjectGetByIdResponse response = getProjectEndpoint().getProjectById(request);
        showProject(response.getProject());
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by id.";
    }

}
