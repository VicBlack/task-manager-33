package ru.t1.kupriyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(projectId, taskId);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().unbindTaskFromProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project";
    }

}
