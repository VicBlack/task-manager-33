package ru.t1.kupriyanov.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.api.endpoint.IEndpointClient;
import ru.t1.kupriyanov.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull
    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(@NotNull final Object data, @NotNull final Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    public Socket connect() throws IOException {
        socket = new Socket(host, port);
        return socket;
    }

    public Socket disconnect() throws IOException {
        socket.close();
        return socket;
    }

}
