package ru.t1.kupriyanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.kupriyanov.tm.dto.request.ServerAboutRequest;
import ru.t1.kupriyanov.tm.dto.request.ServerVersionRequest;
import ru.t1.kupriyanov.tm.dto.response.ServerAboutResponse;
import ru.t1.kupriyanov.tm.dto.response.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
