package ru.t1.kupriyanov.tm.exception.field;

import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.exception.AbstractException;

public class AbstractFiledException extends AbstractException {

    public AbstractFiledException() {
    }

    public AbstractFiledException(@Nullable final String message) {
        super(message);
    }

    public AbstractFiledException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractFiledException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractFiledException(@Nullable final String message, @Nullable final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}