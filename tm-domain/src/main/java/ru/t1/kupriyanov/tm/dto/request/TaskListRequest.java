package ru.t1.kupriyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.TaskSort;

public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    public TaskListRequest(@Nullable final TaskSort sort) {
        this.sort = sort;
    }

}
